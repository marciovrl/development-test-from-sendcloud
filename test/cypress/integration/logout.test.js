describe('Logout Test', () => {
    const userData = {};

    beforeEach(() => {
        cy.visit('');
        cy.createUser().then((user) => {
            userData.username = user.username;
            userData.passwd = user.passwd;
            userData.passwd2 = user.passwd;
        });
        cy.registerUserByWEB(userData);
        cy.login(userData);
    });

    it('must to do logout in successfully', () => {
        cy.logout();
        cy.get('a:contains("Login")').should('exist');
    });
});
