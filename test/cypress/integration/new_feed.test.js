describe('Create new feed Test', () => {
  const userData = {};

  beforeEach(() => {
    cy.visit('');
    cy.createUser().then((user) => {
      userData.username = user.username;
      userData.passwd = user.passwd;
      userData.passwd2 = user.passwd;
    });
    cy.registerUserByWEB(userData);
    cy.login(userData);
  });

  it('must create new feed', () => {
    cy.fixture('feeds').then((feeds) => {
      cy.addFeed(feeds.nu);
    });
    cy.get(".glyphicon").should('exist');
  });

  it('must not create newsfeed without URL', () => {
    cy.addFeed(' ');
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('#error_1_id_feed_url', alert_messages.missing_field_error);
    });
  });

  it('must not create newsfeed with registered URL', () => {
    cy.fixture('feeds').then((feeds) => {
      cy.addFeed(feeds.nu);
    });
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('#error_1_id_feed_url', alert_messages.error_feed_already_registered);
    });
  });
});
