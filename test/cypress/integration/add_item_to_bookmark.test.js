describe('Ability to add a bookmark/favorite to a feed item', () => {
  const userData = {};

  beforeEach(() => {
    cy.visit('');
    cy.createUser().then((user) => {
      userData.username = user.username;
      userData.passwd = user.passwd;
      userData.passwd2 = user.passwd;
    });
    cy.registerUserByWEB(userData);
    cy.login(userData);
  });

  it('must add the feed item to bookmark list', () => {
    cy.registerFeedByWEB()
    cy.addItemToBookmarkList();
    cy.get('.glyphicon-heart').should('exist');
  });

  // TODO ISSUE #5
  it.skip('must remove the feed item to bookmark list', () => {
    cy.addItemToBookmarkList();
    cy.removeItemToBookmarkList();
    cy.get('.glyphicon-heart-empty').should('exist');
  });
});
