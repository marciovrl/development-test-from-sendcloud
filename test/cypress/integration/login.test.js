describe('Login Test', () => {
  const userData = {};

  beforeEach(() => {
    cy.visit('');
    cy.createUser().then((user) => {
      userData.username = user.username;
      userData.passwd = user.passwd;
      userData.passwd2 = user.passwd;
    });
    cy.registerUserByWEB(userData);
  });

  it('must to do login in successfully', () => {
    cy.login(userData);
    cy.get('a:contains("Logout")').should('exist');
  });

  it('must not to do login in successfully', () => {
    userData.passwd = cy.faker.lorem.words();
    cy.login(userData);
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('.alert', alert_messages.login_error);
    });
  });
});
