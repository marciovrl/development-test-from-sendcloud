describe('Ability to add Markdown based comments to the feed items.', () => {
  const userData = {};

  beforeEach(() => {
    cy.visit('');
    cy.createUser().then((user) => {
      userData.username = user.username;
      userData.passwd = user.passwd;
      userData.passwd2 = user.passwd;
    });
    cy.registerUserByWEB(userData);
    cy.login(userData);
  });

  it('must add new markdown to the feed item', () => {
    cy.addCommentToFeed(cy.faker.lorem.words());
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('.alert', alert_messages.success_adding_task);
    });
  });

  // TODO ISSUE #4
  it.skip('must not add new markdown to the feed item without content', () => {
    cy.addCommentToFeed(' ');
    cy.get('.alert').should('exist');
  });
});
