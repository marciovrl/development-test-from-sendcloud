describe('Sign Up Test', () => {
  const userData = {};

  beforeEach(() => {
    cy.visit('');
    cy.createUser().then((user) => {
      userData.username = user.username;
      userData.passwd = user.passwd;
      userData.passwd2 = user.passwd;
    });
  });

  it('must to do sign up in successfully', () => {
    cy.signUp(userData);
    cy.get('a:contains("Logout")').should('exist');
  });

  it('must not to do sign up in successfully if put no username', () => {
    userData.username = ' ';
    cy.signUp(userData);
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('strong', alert_messages.missing_field_error);
    });
  });

  it('must not to do sign up in successfully if put no password confirmation', () => {
    userData.passwd = cy.faker.internet.password();
    cy.signUp(userData);
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('strong', alert_messages.different_password_errors);
    });
  });

  it('must not to do sign up in successfully if put existing user ', () => {
    cy.registerUserByWEB(userData);
    cy.signUp(userData);
    cy.fixture('alert_messages').then((alert_messages) => {
      cy.contains('strong', alert_messages.registered_user_error);
    });
  });
});
