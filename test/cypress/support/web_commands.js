// ***********************************************
// This web_commands.js to create commands to manipulate web page
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// TODO ISSUE #3

// --Login into the application--
Cypress.Commands.add('login', (credentials) => {
  cy.get('.navbar-right > li > a').click();
  cy.get('#id_username').type(credentials.username);
  cy.get('#id_password').type(credentials.passwd);
  cy.get('.bs-component > .btn').click();
});

// --Logout into the application--
Cypress.Commands.add('logout', () => {
  cy.get('a:contains("Logout")').click();
});

// --Fill out the registration form--
Cypress.Commands.add('signUp', (userData) => {
  cy.get('a:contains("Sign Up")').click();
  cy.get('#id_username').type(userData.username);
  cy.get('#id_password1').type(userData.passwd);
  cy.get('#id_password2').type(userData.passwd2);
  cy.get('#submit-id-submit').click();
});

// --Create a new feed register--
Cypress.Commands.add('addFeed', (feed) => {
  cy.get('.btn:contains("New Feed")').click();
  cy.get('#id_feed_url').type(feed);
  cy.get('#submit-id-submit').click();
});

// --Create comments to the feed--
Cypress.Commands.add('addCommentToFeed', (comment) => {
  cy.get('tbody > :nth-child(1) > :nth-child(1) > a').eq(0).click();
  cy.get('td').eq(0).click();
  cy.get('.CodeMirror').type(comment);
  cy.get('#submit-id-submit').click();
});

// --Select item and click in bookmark--
Cypress.Commands.add('addItemToBookmarkList', () => {
  cy.get('tbody > :nth-child(1) > :nth-child(1) > a').eq(0).click();
  cy.get('.glyphicon-heart-empty').click();
});

// --Select item and remove in bookmark--
Cypress.Commands.add('removeItemToBookmarkList', () => {
  cy.get(':nth-child(3) > a').click();
  cy.get('tr > :nth-child(1) > a').click();
  cy.get('[href="/feeds/2/toggle-bookmark/"] > .glyphicon').click();
});
