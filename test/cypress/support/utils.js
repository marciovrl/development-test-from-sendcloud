// --Create user object--
Cypress.Commands.add('createUser', () => {
  const userData = {
    username: cy.faker.name.firstName() + cy.faker.name.lastName(),
    passwd: cy.faker.internet.password(),
  };
  return userData;
});

// --Creates a user record via the graphical (web) interface--
Cypress.Commands.add('registerUserByWEB', (user) => {
  cy.signUp(user);
  cy.logout();
});

// --Creates feedvia the graphical (web) interface--
// TODO ISSUE #8
Cypress.Commands.add('registerFeedByWEB', () => {
  cy.fixture('feeds').then((feeds) => {
    cy.addFeed(feeds.feedburner);
  });
  cy.get('a:contains("All Feeds")').click();
});

// --Creates a user record via API--
// TODO ISSUE #2
Cypress.Commands.add('registerUserByAPI', (user) => {
  cy.get('a:contains("Sign Up")').click();
  cy.getCookies().then((cookies) => {
    const payload = {
      csrfmiddlewaretoken: cookies[0].value,
      username: user.username,
      password1: user.passwd,
      password2: user.passwd,
      submit: 'Submit',
    };
    cy.log(payload.csrfmiddlewaretoken);
    cy.request('POST', 'http://0.0.0.0:8000/accounts/register/', payload);
  });
});
