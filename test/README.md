# Development Test from SendCloud Cypress

## About
Technical test for the position of QA Engineer.

**Notes:** I know that there are several scenarios that I did in Cypress in the e2e tests and that the certain thing would be that in some tests it would be in the unit layer. But to show how Cypress works, I chose to create more scenarios in the e2e layer.

* [About](#About)
* [Preconditions](#Preconditions)
* [Local environment](#Local-environment)
    * [Configuration](#Configuration)
    * [Getting started](#Getting-started)
    * [Install dependencies](#Install-dependencies)
    * [Commands for running the tests](#Commands-for-running-the-tests)
* [Using docker](#Using-docker-environment)
    * [Configuration docker](#Configuration-docker)
    * [Commands for running the tests on docker](#Commands-for-running-the-tests-on-docker)
* [Report](#Report)
* [Links](#Links)
* [Thanks](#Thanks)

## Preconditions
-  [Install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

-  Clone the project: 
`git clone https://gitlab.com/marciovrl/development-test-from-sendcloud`

## Local environment

### Tip
- Don't forget to upload the application locally (docker commands are from the project's README).
- Delete the application database `rm -rf db.sqlite3` after running the tests.

### Configuration
- [Install node](https://www.techgalery.com/2019/12/how-to-install-nodejs-and-npm-on.html)

- [Install NPM](https://www.techgalery.com/2019/12/how-to-install-nodejs-and-npm-on.html)

- Install [Chrome](https://support.google.com/chrome/answer/95346?co=GENIE.Platform%3DDesktop&hl=en-GB) and/or [Firefox](https://support.mozilla.org/en-US/kb/install-firefox-linux)

### Getting started

- Install dependencies: `npm install`

### Commands for running the tests
- Open Cypress console: `npm run cypress:open` - Just go up the cypress console to run the tests, debug and configure.
- Run tests on chrome: `npm run cypress:chrome` - Run the entire suite of tests on chrome headless and after I finish it generates a report in HTML.
- Run tests on firefox: `npm run cypress:firefox` - Run the entire suite of tests on firefox headless and after I finish it generates a report in HTML
- Run lint: `npm run lint:fix <path>` - Performs lint on the desired file and corrects some errors.

## Using docker environment

### Configuration docker
- [Install docker](https://docs.docker.com/get-docker/)
- [Install docker-compose](https://docs.docker.com/compose/install/)

### Commands for running the tests on docker
- Build docker image with tests: `docker-compose build test`
- Run test on the docker: `docker-compose up test`


## Report

### Local report
After running the tests (local machine or in the docker) a report in HTML is automatically generated and stored in `test/cypress/report`.

### Pipeline report 
After executing the test job, the file with the pipeline report is attached to the pipeline.

## Links

### Wiki
Project contains a wiki with one of the Deliverable: "A brief test strategy for the provided application" and also a step by step to create tests in Cypress. [Access here](https://gitlab.com/marciovrl/development-test-from-sendcloud/-/wikis/home)

### Issues
There are some improvements, fixes and ideas for new implementation open as Issue in the project. To follow [ISSUES](https://gitlab.com/marciovrl/development-test-from-sendcloud/-/issues)

**It is important to read because in the code there are some TODO linked to the issues for a better understanding of the project and its improvements.**

### Cypress Documentation
Important support material for new implementations. Read more about this beautiful framework [here](https://docs.cypress.io/guides/overview/why-cypress)


## Thanks
Thanks for the opportunity.